#!/bin/bash

echo "Running in local docker image for running pipeline"

sudo docker run --rm\
                -v $PWD:/code\
                -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 /bin/bash\
                -c "cd /code;\
                    pip3 install -e .;\
                    cd pipelines;\
                    python3 primitive2_pipeline.py;\
                    python3 -m d3m index download -p d3m.primitives.feature_extraction.cnn.UBC -o /code/static; \
                    python3 -m d3m runtime --volumes /code/static fit-score \
                            -p feature_pipeline.json \
                            -r /code/datasets/seed_datasets_current/22_handgeometry/TRAIN/problem_TRAIN/problemDoc.json \
                            -i /code/datasets/seed_datasets_current/22_handgeometry/TRAIN/dataset_TRAIN/datasetDoc.json \
                            -t /code/datasets/seed_datasets_current/22_handgeometry/TEST/dataset_TEST/datasetDoc.json \
                            -a /code/datasets/seed_datasets_current/22_handgeometry/SCORE/dataset_TEST/datasetDoc.json \
                            -o 22_handgeometry_results.csv \
                            -O feature_pipeline_run.yml;\
                    exit"

echo "Done!"
