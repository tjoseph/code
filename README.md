# D3M-TA1 primitive project

A general TA1 project repo for building TA1 primitves or testing `D3M`.

**Note:** This is the working approximate structure.
In order to start building primitves, change the directory/filenames accordingly.

Refer to: [docs](https://gitlab.com/tjoseph/d3m/-/blob/devel/docs/tutorial.rst) for a quick start tutorial.

Supported `D3M` version -`V2020.1.9`

### Structure of repository
The directory and file structure is defined as follows:
```
d3m_test/
  <samplePrimitives>/
    <config_files>
       config_file
    <samplePrimitive1>/
        __init__.py
        <primitive_name>.py
        <primitive_name_test>.py
      .
      .
      .
    <samplePrimitive10>/
          <primitive_name>.py
    <output>/
          <....>
    <generate-primitive-json>.py
  <pipelines>/
  <setup>.py
  <requirements>.txt
```


### Running the code

**Step-0:** Get some seed datasets to run pipeline tests. Run inside the code repository.
```
git lfs clone https://datasets.datadrivendiscovery.org/d3m/datasets.git -X "*"

cd datasets

git lfs pull -I seed_datasets_current/22_handgeometry/
git lfs pull -I seed_datasets_current/38_sick/
```

**Step-1:** Install packages
```
./run-docker.sh

pip3 install -e .
```

**Step-2:** Generate primitive `json`
```
cd samplePrimitives

python3 generate-primitive-json.py

exit
```

**Step-3:** Generate Pipeline
```
./run_pipeline_<select>.sh
```

**Note**: Edit local path inside shell scripts if necessary.

### Code References

1. [USC-ISI TA1 primitives](https://github.com/usc-isi-i2/dsbox-primitives)
2. [Auton Lab TA1 primitives](https://github.com/autonlab/autonbox)
3. [D3M Common TA1 primitives](https://gitlab.com/datadrivendiscovery/common-primitives/tree/master/common_primitives)

`D3M` project repositories that helped in setting up this code base.
