from d3m import container
from d3m.primitive_interfaces import base, transformer
from d3m.metadata import hyperparams

from samplePrimitives.config_files import config

__all__ = ('InputToOutput',)

Inputs = container.DataFrame
Outputs = container.DataFrame

class Hyperparams(hyperparams.Hyperparams):
    """
    No hyper-parameters for this primitive.
    """
    pass

class InputToOutput(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A sample primitive. The output dataframe is the input dataframe.
    """
    __author__ = 'D3M'
    metadata = hyperparams.base.PrimitiveMetadata({
        "id": "dsbox-ensemble-voting",
        "version": config.VERSION,
        "name": "DSBox ensemble voting",
        "description": "A primitive which directly outputs the input",
        "python_path": "d3m.primitives.classification.input_to_output.UBC",
        "primitive_family": "DATA_PREPROCESSING",
        "algorithm_types": ["ENSEMBLE_LEARNING"],
        "source": {
            "name": config.D3M_PERFORMER_TEAM,
            "contact": config.D3M_CONTACT,
            "uris": [config.REPOSITORY]
        },
        "keywords": ["voting", "ensemble"],
        "installation": [config.INSTALLATION],
    })

    def __init__(self, *, hyperparams: Hyperparams) -> None:
        super().__init__(hyperparams=hyperparams)
        self.hyperparams = hyperparams

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        #outputs = pandas.DataFrame((Inputs), generate_metadata=True)

        outputs = inputs

        return base.CallResult(outputs)
